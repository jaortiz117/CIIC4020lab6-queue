import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class TheProcessingSystem {
	
	public static void main(String[] args) throws FileNotFoundException {
		
		Queue<Job> inputQueue = readJobs("input.txt");
		Queue<Job> processingQueue = new LinkedList<>();
		ArrayList<Job> terminatedJobs = new ArrayList<>();
		
		int t = 0;
		
		while((inputQueue.size()+processingQueue.size())>0){
			if(!(processingQueue.isEmpty())){
				Job current = processingQueue.peek();
				current.isServed(1);
				
				if(current.getRemainingTime() == 0){
					current.setDepartureTime(t);
					processingQueue.poll();
					terminatedJobs.add(current);
				}
				else{
					processingQueue.add(processingQueue.poll());
				}
				
			}
			if(!(inputQueue.isEmpty()) && 
					(inputQueue.peek().getArrivalTime() == t)){
				processingQueue.add(inputQueue.poll());
			}
			
			t++;
		}
		
		System.out.println("Average Time in System is: "+ calculateTime(terminatedJobs));
		
	}
	
	private static Queue<Job> readJobs(String fileName) throws FileNotFoundException{
		
		Queue<Job> jobs = new LinkedList<>();
		
		File file = new File(fileName);
		Scanner input = new Scanner(file);
		input.useDelimiter("\\D");
		int id = 1;
		
		while(input.hasNext()){
			int at = input.nextInt();
			input.next();
			int st = input.nextInt();
			
			jobs.add(new Job(id, at, st));
			id++;
		}
		input.close();

		return jobs;
	}
	
	private static double calculateTime(ArrayList<Job> jobList){
		double sum = 0;
		double amount = jobList.size();
		
		//total = dep-arriv+1
		for(Job job : jobList){
			int dep = job.getDepartureTime();
			int arv = job.getArrivalTime();
			
			sum += dep-arv;
		}
		
		return sum/amount;
	}
	
}