package NumberTransformations;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;
import java.util.Set;

public class Main {
	
	private static class Operation implements Cloneable{
		
		int val, rank;
		
		public Operation(int val, int rank) {
			this.val = val;
			this.rank = rank;
		}
		
		public int getVal() {
			return val;
		}
		
		public int getRank() {
			return rank;
		}
		
		public void setVal(int val) {
			this.val = val;
		}
		
		public void setRank(int newRank) {
			this.rank = newRank;
		}
		
		public int add() {
			return val+1;
		}
		
		public int sub() {
			return val-1;
		}
		
		public int mult() {
			return val*3;
		}
		
		public int div() {
			return val/3;
		}
		
		public Operation clone() {
			return new Operation(val, rank);
		}
	}
	
	public static void main(String[] args) {
		
		System.out.println("");
		Scanner in = new Scanner(System.in);
        int A = in.nextInt();
        int B = in.nextInt();
        
        System.out.println(minOpFinder(A, B));
	}

	private static int minOpFinder(int a, int b) {
		Set<Integer> visited = new HashSet<Integer>();
		Queue<Operation> opQueue = new LinkedList<>();
		Operation first = new Operation(a, 0);
		
		opQueue.add(first);
		
		while(!opQueue.isEmpty()) {
			
			Operation current = opQueue.poll().clone();

			//if number is out of range it is not evaluated
			if(current.getVal()>0 && current.getVal()<1000000) {


				if(current.getVal() == b)
					return current.getRank();

				visited.add(current.getVal());

				if(current.add() == b
						|| current.sub() == b
						|| current.mult() == b
						|| current.div() == b) {

					return current.getRank() +1;
				}

				//if number not visited it is added to queue
				if(!visited.contains(current.add())) {
					opQueue.add(new Operation(current.add(), current.getRank()+1));
				}

				if(!visited.contains(current.mult())) {
					opQueue.add(new Operation(current.mult(), current.getRank()+1));
				}
				if(!visited.contains(current.sub())) {
					opQueue.add(new Operation(current.sub(), current.getRank()+1));
				}

				if(!visited.contains(current.div())) {
					opQueue.add(new Operation(current.div(), current.getRank()+1));
				}
			}
		}
		
		return 0;
	}

}
